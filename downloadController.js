import { Router } from "express";
import cors from "cors";

const appRouter = Router()
appRouter.use(cors())

appRouter.route("/").get(async (req, resp) => {
    let mmsi = req.query.mmsi
    let start_date = req.query.start_date
    let end_date = req.query.end_date
    let polygon = req.query.polygon

    if(mmsi===undefined){
        console.log(
            `${polygon}, ${start_date}, ${end_date}`
        )
    }
    if(polygon===undefined){
        console.log(
            `${mmsi}, ${start_date}, ${end_date}`
        )
    }
    
    setTimeout(()=>{return resp.download("/home/michael/Projects/dummy_node_server/text.csv")}, 2000)


})

export default appRouter;