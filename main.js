
import express, { json, urlencoded } from "express";
import bodyParser from "body-parser";
import aboutController from "./aboutController.js"
const app = express();


app.use(json())
app.use(bodyParser.urlencoded({extended: true})).use(bodyParser.json());

import downloadController from './downloadController.js';

app.use('/download', downloadController)
app.use('/about', aboutController)


app.listen(2000, () => console.log("listening at port 2000"))
